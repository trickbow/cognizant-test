import React from 'react';
import './App.css';
import States from './data.json';
import DailyWeather from './DailyWeather';
import CityDisplay from './CityDisplay';
import DateDisplay from './DateDisplay';
import TemperatureDisplay from './TemperatureDisplay';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {data: States, search: null, selectedCity: null, cityList: null, forecast: null}
    this.selectCity = this.selectCity.bind(this);
    this.searchCity = this.searchCity.bind(this);
    this.selectDate = this.selectDate.bind(this);
  }

  componentWillMount(){
    // merge cities into array
      let cityList = [];
      Object.entries(this.state.data.States).forEach(state=>{
        const cities = state[1].cities;
        const currentTime = state[1].time;
        const currentDate = state[1].currentdate;
        cities.forEach(city =>{
          city.temprature = city.forecast[0].temprature;
          city.feels = city.forecast[0].feels;
          city.state = state[0];
          city.time = currentTime;
          city.date = currentDate;
          cityList.push(city);
        })
      });
      this.setState({cityList});
  }

  searchCity=(event)=>{
  	let city = event.target.value;
    this.setState({search:city});
  }

  selectCity = (city) =>{
    window.event.preventDefault();
    this.setState({selectedCity: city});
    this.setState({forecast: city.forecast});
  }

  selectDate = (item) =>{
    window.event.preventDefault();
    this.setState(prevState =>({
      ...prevState,
      selectedCity: {
        ...prevState.selectedCity,
        date: item.Date,
        time: item.Time,
        feels: item.feels,
        temprature: item.temprature
      }
    }));
  }
  
  render() {
    const cityList = this.state.cityList;    

  	const searchedCities = cityList.filter((data) =>{
    	if(this.state.search == null)
          return data
      else if(data.name.toLowerCase().includes(this.state.search.toLowerCase())){
          return data
      }
    });

    return (
      <div>
        <h2>Weather App</h2>
        <input 
          type="text"
          name="search"
          placeholder="Enter City"
          label="Enter City"
          autoComplete="off"
          onChange={(e)=>{this.searchCity(e);document.getElementById("cityList").classList.remove("hide")}}
          onFocus={() => document.getElementById("cityList").classList.remove("hide")}
          onBlur={() => document.getElementById("cityList").classList.add("hide")}
        />
        <div id="cityList" className="hide">
        {searchedCities && 
          searchedCities.map(city =>{
          return (
            <div key={city.name}>
              <a onMouseDown={() => {this.selectCity(city);document.getElementById("cityList").classList.add("hide");document.getElementById("cityList").focus()}}>{city.name}, {city.state}</a>
            </div>
          )
          })
        }
        </div>
        {
          this.state.selectedCity && this.state.selectedCity.name && (
            <div>
              <div className="left">
                <DateDisplay date={this.state.selectedCity.date} />
                <CityDisplay city={this.state.selectedCity.name} cityState={this.state.selectedCity.state} time={this.state.selectedCity.time} />
                <TemperatureDisplay temp={this.state.selectedCity.temprature} feels={this.state.selectedCity.feels} />
              </div>              
            </div>
          )
        }
        <div style={{clear: "both"}}>
          {
          this.state.forecast && 
          this.state.forecast.map((item, index)=>{
            return(
              <div key={index}>
                <a onClick={()=>this.selectDate(item)}>
                  <DailyWeather item={item} boxWidth={100 / this.state.forecast.length} />
                </a>            
              </div>
              )

            })
          }
      </div>
      </div>
    )
  }
}
export default App;
