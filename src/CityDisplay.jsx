import React from 'react';

function CityDisplay(props){
	return(
    <>    
    <h1>{props.city}, {props.cityState}</h1>
    <h4>{props.time}</h4>
    </>
  );
}

export default CityDisplay;