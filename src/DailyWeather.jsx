import React from 'react';

function DailyWeather(props){
	return(
      <div style={{width: props.item.boxWidth, float: "left", padding: "10px"}}>
      {props.item.Date}<br />
      {props.item.Time}<br />
      Temp: {props.item.temprature}<br />
      Feels Like: {props.item.feels}<br />
    </div>
  );
}

export default DailyWeather;