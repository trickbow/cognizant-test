import React from 'react';

function DateDisplay(props){
	return(      
        <h3>{props.date}</h3>
  );
}

export default DateDisplay;