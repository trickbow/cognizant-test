import React from 'react';

function TemperatureDisplay(props){
    return(
        <div>        
            <div className="bigText">Temp: {props.temp}</div>
            <div className="bigText">Feels like: {props.feels}</div>
        </div>
    )
}

export default TemperatureDisplay;